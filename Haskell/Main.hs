module Main where

import System.IO
import System.Cmd (system)
import GHC.IO.Exception (ExitCode(..) )

import Lexer (alexScanTokens, Token (..))
import Parser (parse)
import Syntax (AST)
import Semantics (eval)

main :: IO ()
               
main = 
     do 
     putStr "> "
     hFlush stdout
     str <- getLine
     e <- return $ parse (alexScanTokens str)
     putStr $ show e++"\n"
     putStr $ show (eval e) ++"\n"
     main

