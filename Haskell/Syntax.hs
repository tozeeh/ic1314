module Syntax where

data AST = 
    Num Int
  | Add AST AST
  | Sub AST AST
  | Mul AST AST
  | Div AST AST
  | VTrue
  | VFalse
  deriving (Eq,Show)

