{

module Lexer where

}

%wrapper "basic"

$digit = 0-9			-- digits
$alpha = [a-zA-Z]		-- alphabetic characters
$nl    = [\n\r]                 -- 

tokens :-

  $nl+                                  ; -- not skipping...
  $white+				;
  $digit+				{ \s -> TokenNum (read s) }
  \+  					{ \_ -> TokenPlus }
  \-  					{ \_ -> TokenMinus }
  \*  					{ \_ -> TokenMul }
  \/  					{ \_ -> TokenDiv }
  \(  					{ \_ -> TokenLParen }
  \)  					{ \_ -> TokenRParen }
  true                                  { \_ -> TokenTrue }
  false                                 { \_ -> TokenFalse }


{

data Token = 
       	 TokenNum Int
       | TokenPlus
       | TokenMinus
       | TokenMul
       | TokenDiv
       | TokenLParen
       | TokenRParen
       | TokenTrue
       | TokenFalse
       deriving (Eq, Show )

}
