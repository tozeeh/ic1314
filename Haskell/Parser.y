{
module Parser where

import Lexer
import Syntax
}

%name parse
%tokentype { Token }
%error { parseError }

%token
    num  { TokenNum $$ }
    '+'  { TokenPlus }
    '-'  { TokenMinus }
    '*'  { TokenMul }
    '/'  { TokenDiv }
    '('  { TokenLParen }
    ')'  { TokenRParen }
    true   { TokenTrue }
    false   { TokenFalse }
%%

Start:
       Exp { $1 }
;

Exp : 
       Exp '+' Term                      { Add $1 $3 }
     | Exp '-' Term                      { Sub $1 $3 }
     | Term                              { $1 }
;

Term :
       Term '*' Factor                   { Mul $1 $3 }
     | Term '/' Factor                   { Div $1 $3 }
     | Factor                            { $1 }
;

Factor :
       num                                { Num $1 }
     | '(' Exp ')'                        { $2 }
     | true                               { VTrue }
     | false                              { VFalse }
;

{

parseError :: [Token] -> a
parseError _ = error "Parse error"

}
