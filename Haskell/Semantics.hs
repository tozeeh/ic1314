module Semantics where

import Syntax

eval :: AST -> Int

eval (Num n)    = n
eval (Add e e') = (eval e) + (eval e')
eval (Sub e e') = (eval e) - (eval e')
eval (Mul e e') = (eval e) * (eval e')
eval (Div e e') = (eval e) * (eval e')
eval VTrue = 1
eval VFalse = 0