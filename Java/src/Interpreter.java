import ast.ASTNode;
import parser.ParseException;
import parser.Parser;

public class Interpreter {
	public static void main(String args[]) throws ParseException {
		Parser parser = new Parser(System.in);
		while (true) {
			System.out.print("> ");
			try {
				ASTNode exp = parser.main();
				System.out.println("Ok: "+exp.unparse());
				System.out.println("Ok: "+exp.eval());
			} catch (Error e) {
				System.out.println("Parsing error");
				System.out.println(e.getMessage());
				break;
			} catch (Exception e) {
				System.out.println("NOK.");
				e.printStackTrace();
				break;
			}
		}
	}
}
