package ast;



public class ASTMul implements ASTNode {

	public final ASTNode left;
	public final ASTNode right;
	
	public ASTMul(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public int eval() {
		return left.eval() * right.eval();
	}
	
	@Override
	public String unparse() {
		return "Mul("+left.unparse()+","+right.unparse()+")";
	}

}
