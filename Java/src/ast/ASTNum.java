package ast;



public class ASTNum implements ASTNode {
	public final int num;
	
	public ASTNum(int num) {
		this.num = num;
	}

	@Override
	public int eval() {
		return num;
	}
	
	@Override
	public String unparse() {
		return "Num("+num+")";
	}

}
