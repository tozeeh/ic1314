package ast;


public class ASTAdd implements ASTNode {

	public final ASTNode left;
	public final ASTNode right;
	
	public ASTAdd(ASTNode left, ASTNode right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public int eval() {
		return left.eval() + right.eval();
	}

	@Override
	public String unparse() {
		return "Add("+left.unparse()+","+right.unparse()+")";
	}

}
