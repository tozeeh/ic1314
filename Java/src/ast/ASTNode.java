package ast;


public interface ASTNode {
	int eval();
	
	String unparse();

}
