package ast;


public interface Visitor<T> {
	T visit(ASTNum number);
	T visit(ASTAdd plus);
	T visit(ASTMul mult);
}
