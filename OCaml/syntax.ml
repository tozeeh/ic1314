
type exp = 
	| Number of int 
	| Add of exp * exp
	| Sub of exp * exp 
	| Mul of exp * exp 
	| Div of exp * exp 

type binop_type = Op_add | Op_sub | Op_mul | Op_div | Op_lt

exception SyntaxError of string * string

let rec unparse a =
	match a with
		| Number n -> "Num("^string_of_int n^")"
		| Add (l,r) -> "Add("^(unparse l)^","^(unparse r)^")"
		| Sub (l,r) -> "Sub("^(unparse l)^","^(unparse r)^")"
		| Mul (l,r) -> "Mul("^(unparse l)^","^(unparse r)^")"
		| Div (l,r) -> "Div("^(unparse l)^","^(unparse r)^")"

