type token =
  | EOL
  | INT of (int)
  | ID of (string)
  | PLUS
  | MINUS
  | MULT
  | DIV
  | LPAR
  | RPAR

open Parsing;;
# 2 "parser.mly"
open Syntax

let parse_error s = 
  print_endline s;
  flush stdout
	
let string_of_position p =
	(string_of_int p.Lexing.pos_lnum) ^":" ^ (string_of_int p.Lexing.pos_cnum)

let raiseError () = 
	let p1 = (Parsing.rhs_start_pos 1) in 
  let p2 = (Parsing.symbol_end_pos()) in
	Parsing.clear_parser ();
  raise (SyntaxError(string_of_position p1, string_of_position p2))

# 30 "parser.ml"
let yytransl_const = [|
  257 (* EOL *);
  260 (* PLUS *);
  261 (* MINUS *);
  262 (* MULT *);
  263 (* DIV *);
  264 (* LPAR *);
  265 (* RPAR *);
    0|]

let yytransl_block = [|
  258 (* INT *);
  259 (* ID *);
    0|]

let yylhs = "\255\255\
\001\000\002\000\002\000\002\000\002\000\003\000\003\000\003\000\
\004\000\004\000\000\000"

let yylen = "\002\000\
\002\000\003\000\003\000\001\000\002\000\003\000\003\000\001\000\
\001\000\003\000\002\000"

let yydefred = "\000\000\
\000\000\000\000\000\000\009\000\000\000\011\000\000\000\000\000\
\008\000\005\000\000\000\001\000\000\000\000\000\000\000\000\000\
\010\000\000\000\000\000\006\000\007\000"

let yydgoto = "\002\000\
\006\000\007\000\008\000\009\000"

let yysindex = "\006\000\
\014\255\000\000\000\255\000\000\014\255\000\000\022\255\023\255\
\000\000\000\000\015\255\000\000\013\255\013\255\013\255\013\255\
\000\000\023\255\023\255\000\000\000\000"

let yyrindex = "\000\000\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\255\254\
\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\
\000\000\001\255\008\255\000\000\000\000"

let yygindex = "\000\000\
\000\000\013\000\018\000\019\000"

let yytablesize = 35
let yytable = "\004\000\
\010\000\002\000\004\000\004\000\002\000\002\000\001\000\004\000\
\003\000\002\000\000\000\003\000\003\000\003\000\004\000\004\000\
\003\000\011\000\013\000\014\000\005\000\005\000\012\000\017\000\
\000\000\013\000\014\000\000\000\015\000\016\000\018\000\019\000\
\000\000\020\000\021\000"

let yycheck = "\001\001\
\001\001\001\001\004\001\005\001\004\001\005\001\001\000\009\001\
\001\001\009\001\255\255\004\001\005\001\000\001\002\001\002\001\
\009\001\005\000\004\001\005\001\008\001\008\001\001\001\009\001\
\255\255\004\001\005\001\255\255\006\001\007\001\013\000\014\000\
\255\255\015\000\016\000"

let yynames_const = "\
  EOL\000\
  PLUS\000\
  MINUS\000\
  MULT\000\
  DIV\000\
  LPAR\000\
  RPAR\000\
  "

let yynames_block = "\
  INT\000\
  ID\000\
  "

let yyact = [|
  (fun _ -> failwith "parser")
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 32 "parser.mly"
          ( _1 )
# 112 "parser.ml"
               : Syntax.exp))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'term) in
    Obj.repr(
# 36 "parser.mly"
                ( Add(_1,_3) )
# 120 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'expr) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'term) in
    Obj.repr(
# 37 "parser.mly"
                  ( Sub(_1,_3) )
# 128 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'term) in
    Obj.repr(
# 38 "parser.mly"
       ( _1 )
# 135 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    Obj.repr(
# 39 "parser.mly"
            ( raiseError() )
# 141 "parser.ml"
               : 'expr))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'term) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'fact) in
    Obj.repr(
# 43 "parser.mly"
                 ( Mul(_1,_3) )
# 149 "parser.ml"
               : 'term))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 2 : 'term) in
    let _3 = (Parsing.peek_val __caml_parser_env 0 : 'fact) in
    Obj.repr(
# 44 "parser.mly"
                ( Div(_1,_3) )
# 157 "parser.ml"
               : 'term))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : 'fact) in
    Obj.repr(
# 45 "parser.mly"
       ( _1 )
# 164 "parser.ml"
               : 'term))
; (fun __caml_parser_env ->
    let _1 = (Parsing.peek_val __caml_parser_env 0 : int) in
    Obj.repr(
# 49 "parser.mly"
     ( Number(_1) )
# 171 "parser.ml"
               : 'fact))
; (fun __caml_parser_env ->
    let _2 = (Parsing.peek_val __caml_parser_env 1 : 'expr) in
    Obj.repr(
# 50 "parser.mly"
                 ( _2 )
# 178 "parser.ml"
               : 'fact))
(* Entry main *)
; (fun __caml_parser_env -> raise (Parsing.YYexit (Parsing.peek_val __caml_parser_env 0)))
|]
let yytables =
  { Parsing.actions=yyact;
    Parsing.transl_const=yytransl_const;
    Parsing.transl_block=yytransl_block;
    Parsing.lhs=yylhs;
    Parsing.len=yylen;
    Parsing.defred=yydefred;
    Parsing.dgoto=yydgoto;
    Parsing.sindex=yysindex;
    Parsing.rindex=yyrindex;
    Parsing.gindex=yygindex;
    Parsing.tablesize=yytablesize;
    Parsing.table=yytable;
    Parsing.check=yycheck;
    Parsing.error_function=parse_error;
    Parsing.names_const=yynames_const;
    Parsing.names_block=yynames_block }
let main (lexfun : Lexing.lexbuf -> token) (lexbuf : Lexing.lexbuf) =
   (Parsing.yyparse yytables 1 lexfun lexbuf : Syntax.exp)
;;
